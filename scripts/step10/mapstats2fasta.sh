#!/bin/bash

# 20220325, muellerra, edit fasta headers of prodigal-predicted genes (faa)
# remove Prodigal comment and add bowtie2 mapping statistics

ownName=`basename ${0} .sh`
orfDir="${Project_root}/step08"
statsDir="${Project_root}/step09/mapping_stats"
outDir="${Project_root}/step10"

# Get the plot names from the prodigal dir
plotsAll+=$(ls ${orfDir}/*.faa | awk 'BEGIN {FS="[./_]"} {print $(NF-3)}' | sort -u)

# Log output
exec > >(tee -a ${outDir}/${ownName}.log) 2>&1

# For each plot ...
for plotName in ${plotsAll[@]}; do
	# ... and each timepoint
	for timePoint in T1 T2 T3; do
		if [ -f "${outDir}/${timePoint}_${plotName}_mapped.faa" ]; then
			echo -e "... skipping already existing ${timePoint}_${plotName}_mapped.faa!\n"
		else
			echo -e "\n`date`\t... adding mapping stats to fasta header of ${timePoint}_${plotName}\n"
			cat ${orfDir}/${timePoint}__${plotName}_contigs.predict.faa | \
				seqkit replace -p "\s.+" | \
				seqkit replace --kv-file <(awk '{OFS="\t"} {print $1, " "$4"_"$7}' ${statsDir}/${timePoint}_${plotName}_cov.tsv ) --pattern "^(\w+)" --replacement '${1}{kv}' | \
				seqkit replace --kv-file <(awk '{OFS="\t"} {print $1, "|"$2"_"$3}' ${statsDir}/${timePoint}_${plotName}_onemate_map.tsv ) --pattern "^(\w+)" --replacement '${1}{kv}' | \
				seqkit replace --pattern "\s" --replacement "_" > ${outDir}/${timePoint}_${plotName}_mapped.faa 
		fi
	done
done
