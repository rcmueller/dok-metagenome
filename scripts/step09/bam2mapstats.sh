#!/bin/bash

# 20220325, muellerra
# Get stats: for each bam file: get coverage, filter out mate mappings (147 and 83), write to sam, convert to bam and index, get idxstats

cd ${Project_root}/step09
mkdir onemate_filtered mapping_stats

for inFile in T*.bam; do
        echo "Processing ${inFile}"
        outFile=$(basename ${inFile} _contigs.predict.bam | sed 's/__/_/')
        if [ -f "mapping_stats/${outFile}_cov.tsv" ]; then
                echo -e "\tmapping_stats/${outFile}_cov.tsv exists; skipping"
        else
                echo -e "\tget coverage ..."
                samtools coverage ${inFile} -o mapping_stats/${outFile}_cov.tsv
        fi
        if [ -f "onemate_filtered/${outFile}_onemate.bam" ]; then
                echo -e "\tonemate_filtered/${outFile}_onemate.bam exists; skipping"
        else
                echo -e "\tfilter mate mappings, write to sam ..."
                samtools view --threads 20 -h ${inFile} | awk '$2 !~ /(^147$)|(^83$)/' > onemate_filtered/${outFile}_onemate.sam
                echo -e "\tconvert to bam ..."
                samtools view --threads 20 -h onemate_filtered/${outFile}_onemate.sam -b -o onemate_filtered/${outFile}_onemate.bam
                echo -e "\tcreate index ..."
                samtools index -@ 20 onemate_filtered/${outFile}_onemate.bam
                echo -e "\tremove sam ..."
                rm onemate_filtered/${outFile}_onemate.sam
        fi
        if [ -f "mapping_stats/${outFile}_onemate_map.tsv" ]; then
                echo -e "\tmapping_stats/${outFile}_onemate_map.tsv exists; skipping"
        else
                echo -e "\tget idxstats ..."
                samtools idxstats --threads 20 onemate_filtered/${outFile}_onemate.bam > mapping_stats/${outFile}_onemate_map.tsv
        fi
        echo -e "Processing ${inFile} finished\n"
done
