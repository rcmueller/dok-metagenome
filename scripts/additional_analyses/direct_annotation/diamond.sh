#!/bin/bash

# 20220704, muellerra, DIAMOND and meganizer (background) directly from step05
# Use 60 threads, increase block size, decrease index chunks, decrease e-value
# Wiki: https://github.com/bbuchfink/diamond/wiki/3.-Command-line-options

# Set variables
ownName=`basename ${0} .sh`
diamond="~/bin/diamond"
inDir="${Project_root}/step05"
tmpDir="${Project_root}/temp"
outDir="${Project_root}/step06"
refDat="${Project_root}/step11/nr"
useThreads=60

# Get the plot names from the pre-processed reads dir
plotsAll+=$(ls ${inDir}/*.gz| awk 'BEGIN {FS="[/_]"} {if ($(NF) ~ /R1/) print $(NF-1)}')

# Log output
exec > >(tee -a ${outDir}/${ownName}.log) 2>&1

# Annotation for each plot
for plotName in ${plotsAll[@]}; do
	for t in T1 T2 T3; do
		if [ -f "${outDir}/${t}_${plotName}.daa" ]; then
			echo -e "... skipping already existing ${t}_${plotName}!\n"
		else
			echo -e "\n`date`\t... aligning genes for ${t} ${plotName}\n"
			${diamond} blastx \
				--threads ${useThreads} --block-size 20 --index-chunks 2 --outfmt 100 \
				--tmpdir ${tmpDir} \
				--evalue 0.00001 \
				--taxonlist 2,2157 \
				--db ${refDat} \
				--query ${inDir}/${t}__${plotName}_R1.fastq.gz \
				--query ${inDir}/${t}__${plotName}_R2.fastq.gz \
				--out ${outDir}/${t}_${plotName}.daa
			echo -e "\n`date`\t... finished aligning genes for ${t} ${plotName}\n"
			echo -e "\n`date`\t... calling meganizer.sh ${t}_${plotName}.daa (send to background)\n"
			${outDir}/meganizer.sh ${t}_${plotName}.daa 2>&1 > /dev/null &
		fi
	done
done
