#!/bin/bash

# 20220519, muellerra, extract annotations for all samples and all ontologies
# 20220927, muellerra, adaption to direct annotations (temporarily pull to local SSD)

ownName=`basename ${0} .sh`
${Project_root}
readExtr="~/software/megan/tools/read-extractor"
daa2Info="~/software/megan/tools/daa2info"
daaDir="${Project_root}/step06"
outDir="${Project_root}/step06/extract_direct_annotations"

# Get the sample names from the DIAMOND dir
samplesAll+=$(basename -s .daa -a ${daaDir}/*.daa)

# Log output
exec > >(tee -a ${outDir}/${ownName}.log) 2>&1

# Create temporary directory
if [ ! -d ${outDir}/temp ]; then
	mkdir ${outDir}/temp
fi

# Process each annotation category
for funcAnnot in EC EGGNOG INTERPRO2GO SEED; do

	echo -e "`date`\textracting ${funcAnnot} annotations"
	if [ ! -d ${outDir}/${funcAnnot} ]; then
		mkdir ${outDir}/${funcAnnot}
	fi

	# Process all annotated samples
	for sampleName in ${samplesAll[@]}; do

		# Get count2category
		if [ -f "${outDir}/${funcAnnot}/${sampleName}_${funcAnnot}_c2c.tsv" ]; then
			echo -e "`date`\t... skipping c2c for ${sampleName}, file exists\n"
		else
			echo -e "`date`\t... extracting c2c of ${sampleName}\n"
			${daa2Info} -i ${daaDir}/${sampleName}.daa -c2c ${funcAnnot} | sort -h > ${outDir}/${funcAnnot}/${sampleName}_${funcAnnot}_c2c.tsv
		fi

		# Get paths
		if [ -f "${outDir}/temp/${sampleName}_${funcAnnot}_paths.tsv" ]; then
			echo -e "\n`date`\t... skipping reads to category paths for ${sampleName}, file exists\n"
		else
			echo -e "\n`date`\t... extracting reads to category paths of ${sampleName}\n"
			${daa2Info} -i ${daaDir}/${sampleName}.daa -r2c ${funcAnnot} -p > ${outDir}/temp/${sampleName}_${funcAnnot}_paths.tsv

			# EGGNOG special case
	    	if [ ${funcAnnot} = "EGGNOG" ]; then
				echo -e "\n`date`\t... special case EGGNOG ontology: genes can be assigned to identical terms corresponding to multiple paths -> pick first based on contig names\n"
				mv ${outDir}/temp/${sampleName}_${funcAnnot}_paths.tsv ${outDir}/temp/${sampleName}_${funcAnnot}_paths_all.tsv # keep original file
				awk '!x[$1]++' ${outDir}/temp/${sampleName}_${funcAnnot}_paths_all.tsv > ${outDir}/temp/${sampleName}_${funcAnnot}_paths.tsv
			fi
			# INTERPRO2GO special case
	    	if [ ${funcAnnot} = "INTERPRO2GO" ]; then
				echo -e "\n`date`\t... special case INTERPRO2GO ontology: genes can be assigned to identical terms corresponding to multiple paths -> pick first based on contig names\n"
				mv ${outDir}/temp/${sampleName}_${funcAnnot}_paths.tsv ${outDir}/temp/${sampleName}_${funcAnnot}_paths_all.tsv # keep original file
				awk '!x[$1]++' ${outDir}/temp/${sampleName}_${funcAnnot}_paths_all.tsv > ${outDir}/temp/${sampleName}_${funcAnnot}_paths.tsv
			fi
		fi

		# Get accession numbers
		if [ -f "${outDir}/temp/${sampleName}_${funcAnnot}_acc.tsv" ]; then
			echo -e "\n`date`\t... skipping reads to category accession numbers for ${sampleName}, file exists\n"
		else
			echo -e "\n`date`\t... extracting reads to category accession numbers of ${sampleName}\n"
			${daa2Info} -i ${daaDir}/${sampleName}.daa -r2c ${funcAnnot} > ${outDir}/temp/${sampleName}_${funcAnnot}_acc.tsv
		fi

		# Stitching together paths and accession numbers
		if [ -f "${outDir}/${funcAnnot}/${sampleName}_${funcAnnot}_acc_to_path.tsv" ]; then
			echo -e "\n`date`\t... skipping accession number to path mapping for ${sampleName}, file exists\n"
		else
			echo -e "\n`date`\t... creating accession number to path mapping for ${sampleName}\n"
			# Create acc2path
			paste ${outDir}/temp/${sampleName}_${funcAnnot}_acc.tsv ${outDir}/temp/${sampleName}_${funcAnnot}_paths.tsv | \
				awk 'BEGIN {FS="\t"; OFS="\t"} {if ($1 == $3) {print $1, $2, $4}}' \
				| sort -h -k 2 > ${outDir}/${funcAnnot}/${sampleName}_${funcAnnot}_acc_to_path.tsv
		fi

		echo -e "`date`\t... remove ${daaDir}/${sampleName} from local SSD and compress temp files"
		rm -v ${daaDir}/${sampleName}.daa
		pigz -v ${outDir}/temp/${sampleName}_${funcAnnot}*.tsv

	done

done

echo -e "\n`date`\tScript finished.\n"
exit 0
