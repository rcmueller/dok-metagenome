#!/bin/bash

# muellerra, 20220704, meganize DIAMOND output (blastx alignment of step05 reads)
# Manual: https://software-ab.informatik.uni-tuebingen.de/download/megan6/manual.pdf
 
if [[ $# -ne 1 ]]; then
    echo -e "\nUsage: ./meganizer.sh <daa-file>" >&2
    exit 2
fi

# Set variables
ownName=`basename ${0} .sh`
daaFile="${1}"
daaMeganizer="~/software/megan/tools/daa-meganizer"
daaDir="${Project_root}/step06"
meganMap="${Project_root}/step11/megan-map-Feb2022.db"

# Log output
exec > >(tee -a ${daaDir}/${ownName}.log) 2>&1

## daa-meganizer uses 8 threads by default (but really mostly just 1)
echo -e "\n`date`\t... meganizing ${daaFile}\n"
${daaMeganizer} -v -t 10 -i ${daaDir}/${daaFile} -mdb ${meganMap}
echo -e "\n`date`\t... finished meganizing ${daaFile}\n"
