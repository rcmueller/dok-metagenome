#!/usr/bin/python3

## 20221005, muellerra, dir(ect)annot(ation)_res(ult)gen(eration)
##                      adaptation of fannot_resgen.py

## generate result tables for direct functional annotation of step05 pre-processed reads
## argument: functional annotation category (EC|EGGNOG|INTERPRO2GO|SEED)
## input files for all samples (sampleName) and each category (funcAnnot):
## - sampleName_funcAnnot_acc_to_path.tsv                accession to path
## - sampleName_funcAnnot_c2c.tsv                        category to count
## output: accession number, path (ontology), stat (normalised c2c)

import os
import sys
import glob
import re
import numpy as np
#import statistics
try:
    import pandas as pd
except ImportError:
    subprocess.check_call([sys.executable, "-m", "pip", "install", 'pandas'])
finally:
    import pandas as pd

## Argument handling
if len(sys.argv) != 2:
    print("""\

  Usage: ./dirannot_resgen.py annotation_category

  annotation_category: EC, EGGNOG, INTERPRO2GO, SEED (case insensitive)
  required files:      *_acc_to_path.tsv, *_c2c.tsv

  dirannot_resgen = DIR(ect) ANNOT(ation) RES(ult) GEN(eration):
  create result table based on step05 pre-processed reads annotated with DIAMOND/MEGAN
""")
    sys.exit(1)

funcAnnot = sys.argv[1].upper()
startDir = os.getcwd() 
workDir = startDir + '/' + funcAnnot
try:
    os.chdir(workDir)
except:
    print("Cannot change to " + workDir)

## Prepare sample List (based on accession-to-path files)
print("... prepare sample list")

sampleA2Plist = sorted(glob.glob(workDir + '/*_' + funcAnnot + '_acc_to_path.tsv'))
sampleNames = []
for filePath in sampleA2Plist:
    splitPath = re.split(r'/|_', os.path.basename(filePath))
    ## Optional conditional: all needed files for this sample are available or throw warning/error+exit
    sampleNames.append(splitPath[0] + "_" + splitPath[1])

## Get all unique accession-path values from extracted acc_to_path files, accession as index, drop duplicates, expand path column, drop first and last column
print("... read acc_to_path files (may take a while)")
resTable = pd.concat((pd.read_csv(sampleA2P, sep='\t', header=None, usecols=[1,2], index_col=0) for sampleA2P in sampleA2Plist)).drop_duplicates(keep='first')
resTable = resTable[2].str.split(';', expand=True).iloc[:,1:-1] # omit first (category name) and last column (dispensable ';' produced by MEGAN export at EOL)
resTable = resTable.replace(r'^$', np.nan, regex=True) # Replace blank fields with NA
resTable.index.name = 'accession' # Set name for index column

## Create header dynamically based on number of level columns in annotation category
print("... prepare result tables")
levelHead = []
for level in range(resTable.shape[1]):
    levelHead.append("level_" + str(level + 1))
resTable.columns = levelHead

## Prepare result tables (count to category, normalised counts)
resTableC2C = resTable
resTableCnorm = resTable

print("... calculate sample stats")
for sampleName in sampleNames:
    ## Parse category-to-count data and left join by accession
    print(sampleName + ": processing annotation paths and counts")
    c2cFile = workDir + '/' + sampleName + '_' + funcAnnot + '_c2c.tsv'
    sampleC2C = pd.read_csv(c2cFile, sep='\t', header=None, index_col=0, names=[sampleName + "_c2c"])
    sampleC2C.index.name = 'accession'
    sampleC2C[sampleName + "_c2c"] = sampleC2C[sampleName + "_c2c"].astype('int')
    resTableC2C = resTableC2C.join(sampleC2C, on='accession', how='left', sort=True)
    sumC2C = resTableC2C[sampleName + '_c2c'].sum()
    resTableCnorm[sampleName + '_cnorm'] = (resTableC2C[sampleName + '_c2c'] / (sumC2C / 1000000))

resTableCnorm = resTableCnorm.sort_values(by=['accession'])

## DEBUG
print(resTableC2C)
print(resTableCnorm)

## Write results, na_rep: missing data representation (NA)
print("... write result tables to " + startDir)
os.chdir(startDir)
resTableC2C.to_csv(funcAnnot + '_c2c.tsv', sep='\t', na_rep='NA', header=True)
resTableCnorm.to_csv(funcAnnot + '_cnorm.tsv', sep='\t', na_rep='NA', header=True)
