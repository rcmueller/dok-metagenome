#!/usr/bin/python3

## Generate gene variant number table based on CNPS mapping files and DIAMOND alignment data
## Input: directory of CAZy, NCyc, PCyc or SCyc alignments (*.diamond), corresponding mapping (accession_number-to-gene_id-to-gene_family) and output file

## Changelog
## 20231222, some bug fixes, clean up and add argument for output file
## 20231220, first version

versionSelf = '2023.12.20'

import os
import sys
import glob
#import re
import pandas as pd
#import numpy as np
#import statistics

## Version and argument handling
if len(sys.argv) != 4 or sys.argv[1] == 'help':
    if len(sys.argv) == 2 and sys.argv[1] == 'version':
        print ('\n' + os.path.basename(__file__) + ':\t' + versionSelf + '\n')
        print ('Python:\t' + sys.version)
#        print ('numpy:\t' + np.__version__)
#        print ('pandas:\t' + pd.__version__)
#        print ('re:\t' + re.__version__)
        quit()
    print("""\

  gene_variants_count.py = C, N, P or S gene variant number

  Example:\t./gene_variants_count.py . CAZy_map_20230523.tsv <outfile.tsv>

  \t\t... directory of DIAMOND alignment(s) of CAZy, NCyc, PCyc or SCyc proteins (default BLAST output, expects *.diamond files)
  \t\t... CAZy, NCyc, PCyc or SCyc accession_number-to-gene_id-to-gene_family mapping file
""")
    sys.exit(1)

## Parse arguments
alignPath = sys.argv[1]
alignData = sorted(glob.glob(glob.escape(alignPath) + "/*.diamond"))
sampleNames = list(map(lambda st: os.path.basename(str.replace(st, ".diamond", "")), alignData))
mapFile = sys.argv[2]
outFile = sys.argv[3]

## Prepare accession2gene lookup table
print("... prepare acc2gene lookup table")
geneMap = pd.read_csv(mapFile, sep='\t', header=None, index_col=0, usecols=[0,1]).to_dict()[1] # dict

## Prepare result table and empty list to count contigs aligning to each accession number
resTable = pd.DataFrame(geneMap.items(), columns=['accession', 'gene'])
contigCount = [] # empty list

## Process each alignment file
for sampleName in sampleNames:
    print("... processing", sampleName)
    sampleFile = alignPath + "/" + sampleName + '.diamond'
    sampleAcc = pd.read_csv(sampleFile, sep='\t', header=None, usecols=[1]) # data frame
    ## group by accessions (count contigs aligning to the same accession/sequence)
    contigCount = sampleAcc.groupby(1)[1].count()
    contigCount = pd.DataFrame(contigCount.items(), columns=['accession', sampleName]) # to dataframe for merge
    resTable = resTable.merge(contigCount, how='left', on='accession')

resTable = resTable.drop(['accession'], axis=1) # get rid of accession column
resTable = resTable.groupby(['gene'], as_index=False).sum() # sum up counts for each gene
resTable.to_csv(outFile, sep='\t', na_rep='NA', header=True, index=False)

print ('\nResults written to ' + outFile + '\n')
