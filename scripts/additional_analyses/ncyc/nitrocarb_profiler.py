#!/usr/bin/python3

## 20220929, muellerra: NCyc/CAZy profiler, initial version
## 20230217, muellerra: code cleanup: include versions; output raw count and TPM table
## 20230220: make help, usage, version parsing more robust

## Generate result tables for DIAMOND alignments to NCyc/CAZy proteins
## Input: Alignment type, data (*.diamond), accession 2 gene mapping
## Output: Table with genes and normalised counts (TPM)
## Note: E-values are currently not parsed but could be incorporated to customise different cutoffs
versionSelf = '2023.02.20'

import os
import sys
import glob
import re
import numpy as np
#import statistics
try:
    import pandas as pd
except ImportError:
    subprocess.check_call([sys.executable, "-m", "pip", "install", 'pandas'])
finally:
    import pandas as pd

## Version and argument handling
if len(sys.argv) != 4 or sys.argv[1] == 'help':
    if len(sys.argv) == 2 and sys.argv[1] == 'version':
        print ('\n' + os.path.basename(__file__) + ':\t' + versionSelf + '\n')
        print ('Python:\t' + sys.version)
        print ('numpy:\t' + np.__version__)
        print ('pandas:\t' + pd.__version__)
        print ('re:\t' + re.__version__)
        quit()
    print("""\

  nitrocarb_profiler = Nitrogen or Carbohydrate metabolism profiling

  Example:\t./nitrocarb_profiler.py n "*.diamond" id2gene.map.2019Jul

  \t\t... n for NCyc and c for CAZy data
  \t\t... DIAMOND alignment(s) of NCyc or CAZy proteins, default BLAST output (quote if wildcards are used)
  \t\t... NCyc or CAZy accession number to gene name mapping file

  Versions:\t./nitrocarb_profiler.py version
""")
    sys.exit(1)

## Parse arguments
annotation = sys.argv[1]
if annotation == 'c':
    annotation = 'CAZy'
elif annotation == 'n':
    annotation = 'NCyc'
else:
    print("unknown annotation data:", annotation)
    sys.exit(1)

alignData = sorted(glob.glob(sys.argv[2]))
sampleNames = list(map(lambda st: str.replace(st, ".diamond", ""), alignData))
id2gene = sys.argv[3]

## DEBUG
#print("alignData: ", alignData)
#print("sampleNames: ", sampleNames)

## Parse id2gene mapping file directly into dictionary
print("... parsing", id2gene)
geneMap = pd.read_csv(id2gene, sep='\t', header=None, index_col=0, usecols=[0,1]).to_dict()[1]

## DEBUG
#print(geneMap)

## Prepare result table (counts and TPM), fill with available genes from accession to gene mapping
resTableMap = pd.DataFrame()
resTableMap['gene'] = list(geneMap.values())
resTableMap = resTableMap.drop_duplicates(keep='first')
resTableTPM = resTableMap

## Process each alignment file
for sampleName in sampleNames:
    print("... processing", sampleName)
    sampleFile = sampleName + '.diamond'
    colNames = ['contig', 'accession']
    ## Note: Pandas 1.5.0 can also read tar.gz files with compression='infer'
    contig2acc = pd.read_csv(sampleFile, sep='\t', header=None, index_col=0, usecols=[0,1], names=colNames)

    ## Look up gene name for accession number in mapping file; drop rows if gene=NaN
    contig2acc['gene'] = contig2acc['accession'].map(geneMap)
    contig2acc = contig2acc.dropna(subset='gene')

    ## DEBUG
    #print(contig2acc)
    #print(contig2acc.info(verbose=True))

    ## Expand fasta header and extract mapping stats
    contig2acc['fastaHeader'] = contig2acc.index.str.split('|').str[-1].astype('str')
    contig2acc['ORF_length'] = contig2acc['fastaHeader'].str.split('_').str[0].astype('int')
    contig2acc['mapped'] = contig2acc['fastaHeader'].str.split('_').str[1].astype('int')
    contig2acc['mappedTrue'] = contig2acc['fastaHeader'].str.split('_').str[2].astype('int')
    contig2acc['meandepth'] = contig2acc['fastaHeader'].str.split('_').str[3].astype('float')
    contig2acc = contig2acc.reset_index(drop=True)

    ## Prepare and fill stats dataframe
    gene2stats = pd.DataFrame()
    gene2stats[sampleName + '_' + 'map_sum'] = contig2acc.groupby('gene', sort=False)['mapped'].sum()
    gene2stats[sampleName + '_' + 'ORF_bp_mean'] = contig2acc.groupby('gene', sort=False)['ORF_length'].mean()
    gene2stats[sampleName + '_' + 'map_sum_all'] = contig2acc.groupby('gene', sort=False)['mappedTrue'].sum()
    gene2stats[sampleName + '_' + 'sum_depth'] = contig2acc.groupby('gene', sort=False)['meandepth'].sum()
    gene2stats[sampleName + '_' + 'RPK'] = gene2stats[sampleName + '_' + 'map_sum'] / (gene2stats[sampleName + '_' + 'ORF_bp_mean'] * 0.001)
    sumRPK = gene2stats[sampleName + '_' + 'RPK'].sum()
    #print(sampleName + " sum RPK: " + sumRPK.astype('str'))

    ## DEBUG
    #print(contig2acc.dtypes)
    #print(contig2acc.to_string(max_rows=10))
    #print(contig2acc.info(verbose=True))
    #print(gene2stats)

    ## Prepare result tables (raw counts and calculate TPM based on RPK)
    resTableMap = resTableMap.merge(gene2stats[[sampleName + '_' + 'map_sum']], on='gene', how='left', sort=True).drop_duplicates(keep='first')
    gene2stats[sampleName + '_' + 'TPM'] = (gene2stats[sampleName + '_' + 'RPK'] / (sumRPK / 1000000))
    resTableTPM = resTableTPM.merge(gene2stats[[sampleName + '_' + 'TPM']], on='gene', how='left', sort=True).drop_duplicates(keep='first')
    #print(resTableTPM)


## Write results, na_rep: missing data representation (NA)
print("... write result tables")
resTableMap.to_csv(annotation + '_sumMapped.tsv', sep='\t', na_rep='NA', header=True, index=False)
resTableTPM.to_csv(annotation + '_TPM.tsv', sep='\t', na_rep='NA', header=True, index=False)

print("done.")
