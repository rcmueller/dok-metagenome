#!/bin/bash

# 20220928, muellerra, DIAMOND on NCyc database
# Use 80 threads, increase block size, decrease index chunks, decrease e-value
# Wiki: https://github.com/bbuchfink/diamond/wiki/3.-Command-line-options
versionSelf="2022.09.28"

## Version
if [[ ${1} == "version" ]]; then
    echo -e "\n`basename ${0}`:\t${versionSelf}\n"
    exit 0
fi

# Set variables
ownName=`basename ${0} .sh`
diamond="~/bin/diamond"
inDir="${Project_root}/step10"
tmpDir="${Project_root}/temp"
outDir="${Project_root}/step13"
refDat="${Project_root}/NCyc_100"

# Get the plot names from the prodigal dir
samplesAll+=$(ls ${inDir}/*.faa| awk 'BEGIN {FS="[/_]"} {print $(NF-2)"_"$(NF-1)}')

# Log output
exec > >(tee -a ${outDir}/${ownName}.log) 2>&1

# Alignment for each sample
for sample in ${samplesAll[@]}; do
	if [ -f "${outDir}/${sample}.diamond" ]; then
		echo -e "... skipping already existing ${sample}!\n"
	else
		echo -e "\n`date`\t... aligning genes for ${sample}\n"
		${diamond} blastp \
			--threads 80 --block-size 20 --index-chunks 2 \
			--tmpdir ${tmpDir} \
			--evalue 0.00001 \
			--max-target-seqs 1 \
			--db ${refDat} \
			--query ${inDir}/${sample}_mapped.faa \
			--out ${outDir}/${sample}.diamond
		echo -e "\n`date`\t... finished aligning genes for ${sample}\n"
	fi
done
