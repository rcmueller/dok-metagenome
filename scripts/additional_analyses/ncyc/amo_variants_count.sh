#!/bin/bash

# Count number of amo* variants in NCyc result tables
# 20231218, rmueller

geneList=("amoA_A" "amoA_B" "amoB_A" "amoB_B" "amoC_A" "amoC_B")
sampleList=($(basename --multiple --suffix=.diamond *.diamond))
mapFile="id2gene.map.2019Jul" 
outFile="amo_variants.tsv"

# Header line
printf "%s\t" "gene	${sampleList[@]}" | column > ${outFile}

for gene in "${geneList[@]}"
	do echo "counting ${gene} variants"
	printf "%s\t" "${gene}" >> ${outFile} 
        awk -v gene=${gene} '$2 ~ gene {print $1}' ${mapFile} > ${gene}.txt

	for sample in "${sampleList[@]}"
		do geneCount=$(grep -F -f ${gene}.txt ${sample}.diamond | wc -l)
		printf "%s\t" "${geneCount}" >> ${outFile}
	done

	printf "\n" >> ${outFile}
	rm ${gene}.txt # clean up tmp file

done

sed -i 's/\t$//' ${outFile} # remove last tab-stop

echo -e "\nResults written to ${outFile}\n"
