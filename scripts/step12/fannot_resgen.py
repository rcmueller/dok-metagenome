#!/usr/bin/python3

## 20220111, muellerra, f(unctional)annot(ation)_res(ult)gen(eration)
## 20220504, re-write for alternative pipeline: mapping-annotation
## 20220509, generate result tables for c2c, mean ORF, mapped, all mapped, meandepth, RPK, FPKM, TPM
## 20230119, corrected error (drop_duplicates on mapStats: does not consider index=accession)
## 20230220, allow sample name without .gz suffix in seqstats.txt
## 20230220, code clean-up, added versioning
## 20230220, make help, usage, version parsing more robust
## 20230228, drop rows with mappedTrue=0 (correct ORF_bp_mean calculation: only consider ORFs with mappings; affects tables ORF_bp_mean, RPK, FPKM, TPM)
## 20230228, make regex in seqkit stats file parsing more distinct/robust


## Generate result tables for functional annotation and mappings
## Argument: functional annotation category (EC|EGGNOG|INTERPRO2GO|SEED)
## Input files for all samples (sampleName) and each category (funcAnnot):
## - sampleName_funcAnnot_acc_to_path.tsv                accession to path
## - sampleName_funcAnnot_c2c.tsv                        category to count
## - additionaly: seqstats.txt from step05               raw read counts (for FPKM calculation)
## Output: accession number, path (ontology), stat (c2c|meanORF|sumMapped|sumMappedAll|sumMeandepth|RPK|FPKM|TPM)
versionSelf = '2023.02.28'

import os
import sys
import glob
import re
import numpy as np
#import statistics
try:
    import pandas as pd
except ImportError:
    subprocess.check_call([sys.executable, "-m", "pip", "install", 'pandas'])
finally:
    import pandas as pd

## Version and argument handling
if len(sys.argv) != 2 or sys.argv[1] == 'help' or sys.argv[1] == 'version':
    if len(sys.argv) == 2 and sys.argv[1] == 'version':
        print ('\n' + os.path.basename(__file__) + ':\t' + versionSelf + '\n')
        print ('Python:\t' + sys.version)
        print ('numpy:\t' + np.__version__)
        print ('pandas:\t' + pd.__version__)
        print ('re:\t' + re.__version__ + '\n')
        quit()
    print("""\

  fannot_resgen = f(unctional) annot(ation) res(ult tables) gen(eration):
  Create result tables based on MEGAN annotation and bowtie2 mapping

  Example:\t./fannot_resgen.py ec

  \t\t... annotation_category: EC, EGGNOG, INTERPRO2GO, SEED (case insensitive)
  \t\t... required files:      *_acc_to_path.tsv, *_c2c.tsv, step05/seqstats.txt

  Versions:\t./fannot_resgen.py version
""")
    sys.exit(1)

funcAnnot = sys.argv[1].upper()
startDir = os.getcwd() 
workDir = startDir + '/' + funcAnnot
try:
    os.chdir(workDir)
except:
    print("Cannot change to " + workDir)
    sys.exit(1)

## Prepare sample list (based on accession-to-path files)
print("... prepare sample list")

sampleA2Plist = sorted(glob.glob(workDir + '/*_' + funcAnnot + '_acc_to_path.tsv'))
sampleNames = []
for filePath in sampleA2Plist:
    splitPath = re.split(r'/|_', os.path.basename(filePath))
    # optional conditional: all needed files for this sample are available or throw warning/error+exit
    sampleNames.append(splitPath[0] + "_" + splitPath[1])

## Get all unique accession-path values from extracted acc_to_path files, accession as index, drop duplicates, expand path column, drop first and last column
print("... read acc_to_path files")
resTable = pd.concat((pd.read_csv(sampleA2P, sep='\t', header=None, usecols=[1,2], index_col=0) for sampleA2P in sampleA2Plist)).drop_duplicates(keep='first')
resTable = resTable[2].str.split(';', expand=True).iloc[:,1:-1] # omit first (category name) and last column (dispensable ';' produced by MEGAN export at EOL)
resTable = resTable.replace(r'^$', np.nan, regex=True) # Replace blank fields with NA
resTable.index.name = 'accession' # Set name for index column

## Create header dynamically based on number of level columns in annotation category
print("... prepare result tables")
levelHead = []
for level in range(resTable.shape[1]):
    levelHead.append("level_" + str(level + 1))
resTable.columns = levelHead

## Prepare result tables (count to category, sum of mapped reads, sum of all mapped reads, sum of meandepths, mean ORF length, RPK, FPKM, TPM)
resTableC2C = resTable
resTableMap = resTable
resTableMapAll = resTable
resTableMeanDepth = resTable
resTableORF = resTable
resTableRPK = resTable
resTableFPKM = resTable
resTableTPM = resTable

## Get read counts for FPKM calculation (hardcoded, only R1 and cols 1, 4 from step05 seqstats.txt); brush up table
print("... read seqstats.txt")
step5stats = '../../step05/seqstats.txt'
rawReads = pd.read_csv(step5stats, sep='\t', header=0, index_col=0, usecols=[0,3], skiprows=lambda x: (x != 0) and not x % 2)
rawReads.index = rawReads.index.str.replace(r'__','_', regex=True)
rawReads.index = rawReads.index.str.replace(r'_R1.(fq|fq.gz|fastq|fastq.gz)$','', regex=True)

print("... calculate sample stats")
for sampleName in sampleNames:
    ## Parse category-to-count data and left join by accession
    print(sampleName + ": processing annotation paths and counts")
    c2cFile = workDir + '/' + sampleName + '_' + funcAnnot + '_c2c.tsv'
    sampleC2C = pd.read_csv(c2cFile, sep='\t', header=None, index_col=0, names=[sampleName + "_c2c"])
    sampleC2C.index.name = 'accession'
    sampleC2C[sampleName + "_c2c"] = sampleC2C[sampleName + "_c2c"].astype('int')
    resTableC2C = resTableC2C.join(sampleC2C, on='accession', how='left', sort=True)

    ## Parse mapping stats, set accession as index and left join by accession
    print(sampleName + ": processing mapping stats")
    mapFile = workDir + '/' + sampleName + '_' + funcAnnot + '_acc_to_path.tsv'
    colNames = ['fastaHeader', 'accession']
    mapStats = pd.read_csv(mapFile, sep='\t', header=None, index_col=1, usecols=[0, 1], names=colNames)
    mapStats.index = mapStats.index.astype('int64') # Convert index to int64 for later join to work

    ## Extract mapping stats from fastaHeader (drop contig name, separators=|_)
    mapStats['fastaHeader'] = mapStats['fastaHeader'].str.split('|').str[-1].astype('str')
    mapStats['ORF_length'] = mapStats['fastaHeader'].str.split('_').str[0].astype('int')
    mapStats['mapped'] = mapStats['fastaHeader'].str.split('_').str[1].astype('int')
    mapStats['mappedTrue'] = mapStats['fastaHeader'].str.split('_').str[2].astype('int')
    mapStats['meandepth'] = mapStats['fastaHeader'].str.split('_').str[3].astype('float')

    ## Drop rows with mappedTrue=0 (correct ORF_bp_mean calculation: only consider ORFs with mappings; affects tables ORF_bp_mean, RPK, FPKM, TPM)
    mapStats = mapStats[mapStats.mappedTrue != 0]

    ## Calculate (per accession) mean ORF length, sum mapped, RPK; extract total RPK sum; drop ORF_length and mapped, drop duplicates
    mapStats[sampleName + '_' + 'map_sum'] = mapStats.groupby('accession', sort=False)['mapped'].sum()
    mapStats[sampleName + '_' + 'ORF_bp_mean'] = mapStats.groupby('accession', sort=False)['ORF_length'].mean()
    mapStats[sampleName + '_' + 'map_sum_all'] = mapStats.groupby('accession', sort=False)['mappedTrue'].sum()
    mapStats[sampleName + '_' + 'sum_depth'] = mapStats.groupby('accession', sort=False)['meandepth'].sum()
    mapStats[sampleName + '_' + 'RPK'] = mapStats[sampleName + '_' + 'map_sum'] / (mapStats[sampleName + '_' + 'ORF_bp_mean'] * 0.001)
    mapStats.drop(['fastaHeader', 'ORF_length', 'mapped', 'mappedTrue', 'meandepth'], axis=1, inplace=True)
    #mapStats.drop_duplicates(inplace=True)
    mapStats = mapStats[~mapStats.index.duplicated()]
    sumRPK = mapStats[sampleName + '_' + 'RPK'].sum()
    print(sampleName + " sum RPK: " + sumRPK.astype('str'))

    ## Calculate FPKM (skip parsing avg_len since all reads are between 147.5 - 150)
    rawReadCount = rawReads.at[sampleName, 'num_seqs']
    mapStats[sampleName + '_' + 'FPKM'] = (mapStats[sampleName + '_' + 'RPK'] * 1000000 / rawReadCount)

    ## Calculate TPM based on RPK
    mapStats[sampleName + '_' + 'TPM'] = (mapStats[sampleName + '_' + 'RPK'] / (sumRPK / 1000000))

    ## Left join mapping stats with result table based on accession, drop duplicates
    resTableMap = resTableMap.merge(mapStats[[sampleName + '_' + 'map_sum']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableORF = resTableORF.merge(mapStats[[sampleName + '_' + 'ORF_bp_mean']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableMapAll = resTableMapAll.merge(mapStats[[sampleName + '_' + 'map_sum_all']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableMeanDepth = resTableMeanDepth.merge(mapStats[[sampleName + '_' + 'sum_depth']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableRPK = resTableRPK.merge(mapStats[[sampleName + '_' + 'RPK']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableFPKM = resTableFPKM.merge(mapStats[[sampleName + '_' + 'FPKM']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableTPM = resTableTPM.merge(mapStats[[sampleName + '_' + 'TPM']], on='accession', how='left', sort=True).drop_duplicates(keep='first')

    ##### debug #####
    ## print(mapStats)
    ## print(mapStats.index.dtype)

## Write results, na_rep: missing data representation (NA)
print("... write result tables to " + startDir)
os.chdir(startDir)
resTableC2C.to_csv(funcAnnot + '_c2c.tsv', sep='\t', na_rep='NA', header=True)
resTableMap.to_csv(funcAnnot + '_sumMapped.tsv', sep='\t', na_rep='NA', header=True)
resTableORF.to_csv(funcAnnot + '_meanORF.tsv', sep='\t', na_rep='NA', header=True)
resTableMapAll.to_csv(funcAnnot + '_sumMappedAll.tsv', sep='\t', na_rep='NA', header=True)
resTableMeanDepth.to_csv(funcAnnot + '_sumMeandepth.tsv', sep='\t', na_rep='NA', header=True)
resTableRPK.to_csv(funcAnnot + '_RPK.tsv', sep='\t', na_rep='NA', header=True)
resTableFPKM.to_csv(funcAnnot + '_FPKM.tsv', sep='\t', na_rep='NA', header=True)
resTableTPM.to_csv(funcAnnot + '_TPM.tsv', sep='\t', na_rep='NA', header=True)

print("done.")
