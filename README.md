## Description

The metagenome data processing pipeline steps are documented in the [Wiki page](https://gitlab.com/rcmueller/dok-metagenome/-/wikis/).

There, external software, including download links and references, are linked.

Self-written tools (shell-and Python scripts) can be downloaded here.

In order to run the scripts, please make sure to export your project root directory:

```shell
export DOK_root="<your-project-root-dir>"
```

Please cite ["Organic cropping systems alter metabolic potential and carbon, nitrogen and phosphorus cycling capacity of soil microbial communities"](https://doi.org/10.1016/j.soilbio.2025.109737) if you find this resource useful for your project.

